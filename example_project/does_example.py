#import utils.useful_thing
from .utils import useful_thing as useful_ref

class Example:

   def ex_method(self):

      print("This is an example method")
      useful = useful_ref.UsefulClass()
      useful_variable = useful.useful_method()
      print(useful_variable)

      return

