## Project Info
This project shows how to properly import modules into a unit test.

This project is a result of the question asked on [reddit](https://www.reddit.com/r/learnpython/comments/kedh0v/modulenotfounderror_when_importing_my_scripts_for/).

Hats off to [marko312](https://www.reddit.com/user/marko312/) for the help.

An extremely helpful resource is also [patricksoftwareblog](https://www.patricksoftwareblog.com/python-unit-testing-structuring-your-project/?unapproved=1717&moderation-hash=43c57c149bca7a52fac07e80594b8537).
## Running The Test

**Method 1)** Without using `unittest`

```
anon@anon-MS-7681:/srv/git/example-project$ python3
Python 3.8.5 (default, Jul 28 2020, 12:59:40) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import tests.test_does_example
>>> test = tests.test_does_example.Example()
>>> test.ex_method()
This is an example method
This is a useful variable
```

**Method 2)** Using `unittest`
```
anon@anon-MS-7681:/srv/git/example-project$ python3 -m unittest discover -v
test_1 (tests.test_does_example.TestBasicFunction) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
```
